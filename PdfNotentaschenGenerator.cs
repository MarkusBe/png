using System.Text;
using System.Xml.Schema;
using System.Xml.Linq;

class XmlTexGenerator
{
    XDocument xml;
    String tex, dir;

    private static string IntToLabel(int i)
    {
        const string decimalToLetter = "ABCDEFGHIJ";
        string str = string.Empty;

        foreach (char c in i.ToString("D", System.Globalization.CultureInfo.InvariantCulture))
            str += decimalToLetter[Int32.Parse(c.ToString())];

        return str;
    }

    public XmlTexGenerator(String xmlfile)
    {
        dir = Path.GetDirectoryName(xmlfile);

        if (null == dir)
            dir = ".";

        string templfile = Path.Combine(dir, "Template.tex");
        if (!File.Exists(templfile))
            throw new Exception("Die Datei Template.tex wird im Ordner der XML-Eingabedatei erwartet.");

        tex = File.ReadAllText(templfile);
        xml = XDocument.Parse(File.ReadAllText(xmlfile));
    }

    public bool ValidateXml()
    {
        bool r = true;

        XmlSchemaSet schema = new XmlSchemaSet();
        schema.Add(null, "PdfNotentaschenGenerator.xsd");

        xml.Validate(schema, (o, e) => {
            Console.WriteLine("Fehler in XML-Eingabedatei:");
            Console.WriteLine(o.GetType().Name);
            Console.WriteLine(e.Message);
            r = false;
        });

        return r;
    }

    private void RenderTitlePage()
    {
        var el = xml.Root.Element("Titelseite");

        tex = tex.Replace("[TITEL]", el.Element("Titel").Value);
        tex = tex.Replace("[UNTERTITEL]", el.Element("Untertitel").Value);
        tex = tex.Replace("[TITELBILD]", el.Element("Titelbild").Value);
        tex = tex.Replace("[STAND]", el.Element("Stand").Value);
    }

    private void RenderContent()
    {
        StringBuilder sb = new StringBuilder();
        int i = 0;

        foreach (var el in xml.Descendants("Stimme"))
        {
            string id = el.Attribute("id").Value;
            string name = el.Attribute("name").Value;
            string rank = el.Attribute("rank").Value;

            if (el.Attribute("onnewpage") != null)
                if (el.Attribute("onnewpage").Value.ToLower().Equals("true"))
                    sb.Append(@"\addtocontents{toc}{\protect\pagebreak}");

            sb.Append(@"\section{\LARGE ");
            sb.Append(name);
            sb.AppendLine(@"}");

            foreach (var el2 in xml.Descendants("Notenbuchgruppe"))
            {
                var groupRefs = new List<string>();

                foreach (var x in el2.Descendants("Notenbuch"))
                    groupRefs.Add(x.Attribute("id").Value);

                foreach (var el3 in el2.Descendants("Notenbuch"))
                {
                    string id2 = el3.Attribute("id").Value;
                    string name2 = el3.Attribute("name").Value;

                    sb.Append(@"\subsection{\LARGE ");
                    sb.Append(name2);
                    sb.AppendLine(@"}");

                    sb.Append(@"\label{sec:");
                    sb.Append(String.Format("{0}{1}_{2}", id, rank, id2));
                    sb.Append(@"}");
                    sb.AppendLine(@"\Large");
                    sb.AppendLine(@"Gehe zu ");

                    foreach (string book in groupRefs)
                    {
                        if (!book.Equals(id2))
                        {
                            sb.Append(@"\textbf{\hyperref[");
                            sb.Append(String.Format("sec:{0}{1}_{2}", id, rank, book));
                            sb.Append(@"]{\mbox{");
                            sb.Append(String.Format("{0}", book));
                            sb.AppendLine(@"}}}, ");
                        }
                    }

                    foreach (var el5 in xml.Descendants("Stimme"))
                    {
                        string id3 = el5.Attribute("id").Value;
                        string rank2 = el5.Attribute("rank").Value;
                        string name3 = el5.Attribute("name").Value;

                        if (!name3.Equals(name))
                        {
                            sb.Append(@"\textbf{\hyperref[");
                            sb.Append(String.Format("sec:{0}{1}_{2}", id3, rank2, id2));
                            sb.Append(@"]{\mbox{");
                            sb.Append(String.Format("{0}", name3));
                            sb.AppendLine(@"}}}, ");
                        }
                    }

                    sb.AppendLine(@"\textbf{\hyperref[sec:ToC]{Stimmenverzeichnis}}.");
                    sb.AppendLine();

                    sb.AppendLine(String.Format("\\newcommand{{\\returnto{0}}}{{", IntToLabel(i)));
                    sb.AppendLine(String.Format("  \\hyperref[sec:{0}{1}_{2}]{{", id, rank, id2));
                    sb.AppendLine(@"    \makebox[\paperwidth][c]{");
                    sb.AppendLine(@"      \raisebox{0pt}[\halfpage][0pt]{");
                    sb.AppendLine(@"        \makebox[\paperheight]{");
                    sb.AppendLine(@"          \phantom{\rule{\paperheight}{\halfpage}}");
                    sb.AppendLine(@"        }");
                    sb.AppendLine(@"      }");
                    sb.AppendLine(@"    }");
                    sb.AppendLine(@"  }");
                    sb.AppendLine(@"}");
                    sb.AppendLine();

                    sb.AppendLine(@"\begin{enumerate}");

                    foreach (var el4 in el3.Descendants("Eintrag"))
                    {
                        sb.Append(@"\item[\textbf{");
                        sb.Append(el4.Attribute("no").Value);
                        sb.Append(@".}] \hyperlink{");
                        sb.Append(String.Format("{0}/{1}/{2}/{3}.pdf.1", id2, id, rank, el4.Attribute("page").Value));
                        sb.Append(@"}{");
                        sb.Append(el4.Attribute("name").Value);
                        sb.AppendLine(@"}");
                    }

                    sb.AppendLine(@"\end{enumerate}");

                    for (int j=0; j<1000; j++)
                    {
                        if (File.Exists(Path.Combine(dir, id2, id, rank, String.Format("{0}.pdf", j))))
                        {
                            sb.Append(String.Format("\\includepdf[pages={{-}}, link=true, pagecommand={{\\returnto{0}}}]{{", IntToLabel(i)));
                            sb.Append(String.Format("{0}/{1}/{2}/{3}.pdf", id2, id, rank, j));
                            sb.AppendLine(@"}");
                        }
                    }

                    sb.AppendLine(@"\newpage");
                    i++;
                }
            }
        }

        tex = tex.Replace("[NOTEN]", sb.ToString());
    }

    public string Render()
    {
        RenderTitlePage();
        RenderContent();

        return tex;
    }
}

class Program
{
    static void Main(string[] args)
    {
        if (args.Length < 1)
        {
            Console.WriteLine("Fehlender Parameter für Eingabe: <Notentasche>.xml!");
            Console.WriteLine("Benutzung: (PdfNotentaschenGenerator|dotnet run --) <Notentasche>.xml [<Notentasche>.tex]");

            return;
        }

        try 
        {
            XmlTexGenerator generator = new XmlTexGenerator(args[0]);

            if (generator.ValidateXml())
            {
                if (args.Length < 2)
                    Console.WriteLine(generator.Render());
                else
                    File.WriteAllText(args[1], generator.Render());
            }

        }
        catch (Exception e)
        {
            Console.WriteLine("Fehler bei der TeX-Generierung: {0}", e.Message);
        }
    }
}
