# PdfNotentaschenGenerator

Ein einfaches Kommandozeilenprogramm (C#/.NET) zur Erzeugung von LaTeX-Quellcode  
aus einer XML-Eingabe. Die Struktur der XML-Datei sollte selbsterklärend sein,  
wenn man weiß, wofür das Tool gedacht ist. Eine Schema-Datei liegt bei.  
Notenbuchgruppen sorgen dafür, dass in den Verzeichnissen zusammenghöriger Notenbücher  
untereinander gewechselt werden kann. Mit dem optionalen Attribut 'onnewpage' kann ein  
ungünstiger Seitenumbruch im Stimmenverzeichnis verhindert werden.  

Die LaTeX-Quellcodevorlage Template.tex wird im Ordner der XML-Eingabedatei erwartet.  

Die einzubindenden Notenseiten müssen als PDF relativ zur XML-Eingabedatei mit  
folgender Struktur abgelegt werden: ./Notenbuch-ID/Stimmen-ID/Rank-No/Page-No.pdf  

Wenn die Notenbücher als zusammenhängende PDFs vorliegen, müssen diese zuvor in  
Einzelseiten aufgetrennt werden mit der Seitenzahl als Name (beginnend mit '1.pdf').  
Dazu kann pdftk verwendet werden:  
  pdftk Notenbuch.pdf burst output %d.pdf  

Im Anschluss an die Generierung sollte aus dem LaTeX-Quellcode ein PDF mittels pdflatex  
erzeugt werden können. Das erzeugte PDF enthält alle Noten mit Inhaltsverzeichnis  
und Querverlinkungen, so dass man darin auf einem Kindle oder Tablet navigieren kann.  

## Anforderungen

* Windows, Linux oder macOS  
* .NET SDK (z.B. 6.0)
* pdflatex sowie MiKTeX oder TeX Live  

## Installation

* Linux (z.B. Debian/Ubuntu):  
  sudo apt update  
  sudo apt install texlive-full dotnet-sdk-6.0 -y  
  Optional: dotnet publish -c Release -r linux-x64 --sc  

* macOS:  
  brew install texlive  
  brew install --cask dotnet-sdk  
  Optional: dotnet publish -c Release -r osx-x64 --sc  

* Windows:  
  Auf Webseiten von Microsoft und MiKTeX gehen und dort .net Core SDK und  
  MiKTeX herunterladen/installieren.  
  Optional: dotnet publish -c Release -r win10-x64 --sc  

## Ausführung
Allgemein: (PdfNotentaschenGenerator | dotnet run --) <Notentasche.xml> [<Notentasche.tex>]  

Beispiel:  
TromFlue.xml-Datei anpassen und Notenseiten-PDFs entsprechend in Ordnerstruktur ablegen.  
Änderungen am Layout können direkt in der Template.tex vorgenommen werden.  

Danach im Hauptordner folgendes aufrufen (damit das Inhaltsverzeichnis erzeugt wird,  
muss pdflatex 2x ausgeführt werden):  

* Linux/macOS/Windows mit .NET Runtime/SDK:  
  dotnet run -- TromFlue/TromFlue.xml TromFlue/TromFlue.tex  
  cd TromFlue  
  pdflatex TromFlue.tex  
  pdflatex TromFlue.tex  

* Linux/macOS/Windows ohne .NET Runtime/SDK mit Self-Contained Executable:  
  PdfNotentaschenGenerator TromFlue/TromFlue.xml TromFlue/TromFlue.tex  
  cd TromFlue  
  pdflatex TromFlue.tex  
  pdflatex TromFlue.tex  
