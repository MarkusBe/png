#!/bin/bash
echo "#!/bin/bash" > _ReplaceMusicSheetsWithBlankPdfs.sh
find . -name "*.pdf" | grep -v "blank.pdf" | sed -E "s/(.*)/cp -vf blank.pdf \1/g" >> _ReplaceMusicSheetsWithBlankPdfs.sh
chmod +x _ReplaceMusicSheetsWithBlankPdfs.sh
bash _ReplaceMusicSheetsWithBlankPdfs.sh
rm -f _ReplaceMusicSheetsWithBlankPdfs.sh
